import http from 'http';
import https from 'https';

export type Response = { res: http.IncomingMessage, body: string };

export async function call(url: URL, options: https.RequestOptions, body?: any) {
  return new Promise<Response>((resolve,reject) => {
      const req = https.request({
        ...options,
        hostname: url.hostname,
        path: url.pathname,
        protocol: url.protocol,
        hash: url.hash,
        searchParams: url.searchParams,
      }, res => {
        const chunks: any[] = [];
        res.on('data', data => chunks.push(data));
        res.on('end', () => {
          resolve({ res, body: Buffer.concat(chunks).toString()});
        });
        res.on('error', reject);
      })
      req.on('error', reject);
      if(body) {
        req.write(body);
      }
      req.end();
  })
}
