import * as dotenv from 'dotenv';
import { z } from 'zod';

dotenv.config();

export const ConfigSchema = z.object({
  APP_PROTOCOL: z.string().optional().default('http'),
  APP_HOSTNAME: z.string().optional().default('localhost'),
  APP_PORT: z.string().optional().default('8000'),
  APP_WALLET_BASE_URL: z.string().optional().default('http://localhost:3000/'),
  APP_CONSUMER_WEBID: z.string(),
  APP_PROVIDER_WEBID: z.string(),
  APP_CLIENT_ID: z.string(),
  APP_CLIENT_SECRET: z.string(),
});

const parsed = ConfigSchema.parse(process.env);

export const config = {
  ...parsed,
  APP_BASE_URL: new URL(`${parsed.APP_PROTOCOL}://${parsed.APP_HOSTNAME}:${parsed.APP_PORT}`),
};
