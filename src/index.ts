import http from 'http';
import express from 'express';
import { config } from './Config';
import indexController from './controllers/index';
import cbController from './controllers/cb';

var path = require('path')
const app = express();
app.use(express.json());
//app.set('views',path.join(__dirname, 'views'))
app.set('view engine', 'ejs');
app.get('/', indexController);
app.get('/cb', cbController);
app.use(express.static('public'));

const server = http.createServer(app);
server.listen(config.APP_PORT, () => {
  console.log(`Listening on ${config.APP_BASE_URL.toString()}`);
});
