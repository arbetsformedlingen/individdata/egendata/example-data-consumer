import { config } from './Config';
import { createDpopJWT } from './solid';
import * as client from './client';
import { JWK, KeyLike } from 'jose';
import * as N3 from 'n3';
import { z } from "zod";

const DataRequestSchema = z.object({
  requestorWebId: z.string(),
  providerWebId: z.string(),
  documentType: z.string(),
  returnUrl: z.string(),
  purpose: z.string(),
  documentTitle: z.string(),
});

export type DataRequest = z.infer<typeof DataRequestSchema>;

export const createDataRequestLink = (dataRequest: DataRequest) => {
  const payload = encodeURIComponent(Buffer.from(JSON.stringify(dataRequest), 'utf8').toString('base64'));
  return new URL(`${config.APP_WALLET_BASE_URL}request?payload=${payload}`);
};

export const findStorageUrl = async (webid: URL, privateKey: KeyLike, publicKeyJWK: JWK, accessToken: string) => {
  const dpop = await createDpopJWT(privateKey, publicKeyJWK, 'GET', webid);
  const { body } = await client.call(webid, {
    method: 'GET',
    headers: {
      Authorization: `DPoP ${accessToken}`,
      dpop,
    },
  });

  const store = new N3.Store(new N3.Parser().parse(body));
  const objects = store.getObjects(webid.toString(), 'http://www.w3.org/ns/pim/space#storage', null);
  if (objects.length === 0) throw new Error('WebID agent missing storage predicate');
  if (objects.length > 1) throw new Error('Support for multiple storage predicates is not implemented');
  return new URL(objects[0].value);
}

export const findResourcesInInbox = async (storage: URL, privateKey: KeyLike, publicKeyJWK: JWK, accessToken: string) => {
  const url = new URL(`${storage.toString()}egendata/inbox/`);
  console.log('inbox:', url);
  const dpop = await createDpopJWT(privateKey, publicKeyJWK, 'GET', url);
  const { body } = await client.call(url, {
    method: 'GET',
    headers: {
      Authorization: `DPoP ${accessToken}`,
      dpop,
    },
  });

  const store = new N3.Store(new N3.Parser().parse(body));
  const objects = store.getObjects('', 'http://www.w3.org/ns/ldp#contains', null);
  return objects.map(obj => new URL(`${url}${obj.value}`)); // TODO: Change this to build the url with some path util that can concatenate relative links.
}

export const fetchDataUrlFromInbox = async (inboxResourceUrl: URL, privateKey: KeyLike, publicKeyJWK: JWK, accessToken: string) => {
  const dpop = await createDpopJWT(privateKey, publicKeyJWK, 'GET', inboxResourceUrl);
  const { body } = await client.call(inboxResourceUrl, {
    method: 'GET',
    headers: {
      Authorization: `DPoP ${accessToken}`,
      dpop,
    },
  });

  const store = new N3.Store(new N3.Parser().parse(body));
  const objects = store.getObjects('', 'https://egendata.se/schema/core/v1#OutboundDataRequest', null);
  if (objects.length === 0) throw new Error('Inbox resource missing OutboundDataRequest predicate');

  const dataResponseUrl = new URL(objects[0].value);
  
  console.log('The shared resource is located here:', dataResponseUrl);
  return dataResponseUrl;
}
export const fetchData = async (dataUrl: URL, privateKey: KeyLike, publicKeyJWK: JWK, accessToken: string) => {
  const dpop = await createDpopJWT(privateKey, publicKeyJWK, 'GET', dataUrl);
  const { body } = await client.call(dataUrl, {
    method: 'GET',
    headers: {
      Authorization: `DPoP ${accessToken}`,
      dpop,
    },
  });

  const store = new N3.Store(new N3.Parser().parse(body));
  const objects = store.getObjects('', 'https://egendata.se/schema/core/v1#document', null);
  if (objects.length === 0) throw new Error('Data resource missing document predicate');

  const b64document = objects[0].value;

  const doc = JSON.parse(Buffer.from(b64document.toString(), "base64").toString("utf8"));
  
  console.log('The shared resource is located here:', doc);
  return doc;
}

export const deleteInboxResource = async (resourceUrl: URL, privateKey: KeyLike, publicKeyJWK: JWK, accessToken: string) => {
  const dpop = await createDpopJWT(privateKey, publicKeyJWK, 'DELETE', resourceUrl);
  const { res } = await client.call(resourceUrl, {
    method: 'DELETE',
    headers: {
      Authorization: `DPoP ${accessToken}`,
      dpop,
    },
  });
  console.log('DELETE, response status code:', res.statusCode);
}
