import { exportJWK, generateKeyPair, JWK, KeyLike, SignJWT } from 'jose';
import * as N3 from 'n3';
import { v4 as uuid } from 'uuid';
import * as client from './client';
import { config } from './Config';

type Method = 'GET' | 'POST' | 'PUT' | 'DELETE' | 'HEAD';

export async function createKeyPairAndPublicJWK() {
  const keyPair = await generateKeyPair('ES256');
  const publicKeyJWK = await exportJWK(keyPair.publicKey);
  publicKeyJWK.alg = 'ES256';
  return {
    keyPair,
    publicKeyJWK,
  }
}

export async function createDpopJWT(privateKey: KeyLike, publicKeyJWK: JWK, method: Method, url: URL) {
  return new SignJWT({
      htu: url.toString(),
      htm: method.toUpperCase(),
      jti: uuid(),
  })
    .setProtectedHeader({
      alg: 'ES256',
      jwk: publicKeyJWK,
      typ: "dpop+jwt",
  })
      .setIssuedAt()
      .sign(privateKey);
}

export async function fetchAccessToken(webid: URL, privateKey: KeyLike, publicKeyJWK: JWK) {
  let oidcIssuerBaseUrl: URL;
  {
    const { body } = await client.call(webid, { method: 'GET' });
    const store = new N3.Store(new N3.Parser().parse(body));
    const oidcIssuers = store.getObjects(webid.toString(), 'http://www.w3.org/ns/solid/terms#oidcIssuer', null);
    if (oidcIssuers.length === 0) throw new Error('WebID agent missing oidcIssuer predicate');
    if (oidcIssuers.length > 1) throw new Error('Support for multiple oidc issuer predicates is not implemented');
    oidcIssuerBaseUrl = new URL(oidcIssuers[0].value);
  }

  let oidcConfiguration;
  {
    const { body } = await client.call(new URL('/.well-known/openid-configuration', oidcIssuerBaseUrl), { method: 'GET'});
    oidcConfiguration = JSON.parse(body);
  }

  let accessToken;
  {
    const authString = `${encodeURIComponent(config.APP_CLIENT_ID)}:${encodeURIComponent(config.APP_CLIENT_SECRET)}`;
    const tokenEndpoint = new URL(oidcConfiguration['token_endpoint']);
    const { body } = await client.call(tokenEndpoint, {
      method: 'POST',
      headers: {
        authorization: `Basic ${Buffer.from(authString).toString('base64')}`,
        dpop: await createDpopJWT(privateKey, publicKeyJWK, 'POST', tokenEndpoint),
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    }, 'grant_type=client_credentials&scope=webid');
    accessToken = JSON.parse(body)['access_token'];
  }
  return accessToken;
};
