import { Request, Response } from 'express';
import { config } from '../Config';
import { deleteInboxResource, fetchData, fetchDataUrlFromInbox, findResourcesInInbox, findStorageUrl } from '../egendata';
import { createKeyPairAndPublicJWK, fetchAccessToken } from '../solid';

export default (req: Request, res: Response) => {
  const handle = async () => {
    const { keyPair, publicKeyJWK } = await createKeyPairAndPublicJWK();
    const accessToken = await fetchAccessToken(new URL(config.APP_CONSUMER_WEBID), keyPair.privateKey, publicKeyJWK);
    
    // Find storage url
    const storage = await findStorageUrl(new URL(config.APP_CONSUMER_WEBID), keyPair.privateKey, publicKeyJWK, accessToken);
    console.log('storage:', storage);
    
    // find all inbox resource urls
    const inboxResourceUrls = await findResourcesInInbox(storage, keyPair.privateKey, publicKeyJWK, accessToken);
    console.log('inboxResourceUrls:', inboxResourceUrls);

    const dataResponseUrls = await Promise.all(inboxResourceUrls.map(async (inboxResourceUrl) => {
      const dataResource = await fetchDataUrlFromInbox(inboxResourceUrl, keyPair.privateKey, publicKeyJWK, accessToken);
      return dataResource;
    }));
    const firstInboxResourceUrl = inboxResourceUrls[0];

    if (firstInboxResourceUrl) {
      console.log('dataResponseUrls:', dataResponseUrls);
      const firstDataResponseUrl = dataResponseUrls[0];
  
      const data = await fetchData(firstDataResponseUrl, keyPair.privateKey, publicKeyJWK, accessToken);   
  
      await deleteInboxResource(firstInboxResourceUrl, keyPair.privateKey, publicKeyJWK, accessToken);
      res.render('cb', {data});
      return;
    }
    res.send('no data found.')
  };
  handle();
};
