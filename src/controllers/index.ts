import { Request, Response } from 'express';
import { config } from '../Config';
import { createDataRequestLink } from '../egendata';

export default (req: Request, res: Response) => {
  res.render('index', {
    link: createDataRequestLink({
      requestorWebId: config.APP_CONSUMER_WEBID,
      providerWebId: config.APP_PROVIDER_WEBID,
      documentType: 'https://egendata.se/schema/core/v1#JobSeekerRegistrationStatus',
      returnUrl: new URL('/cb', config.APP_BASE_URL).toString(),
      purpose: 'Example purpose',
      documentTitle: 'Example document title',
    }),
  });
};
