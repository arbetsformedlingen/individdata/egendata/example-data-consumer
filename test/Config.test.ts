import { afterEach, describe, it, vi, expect } from 'vitest';
import { config } from '../src/Config';

describe('Config', () => {
  afterEach(() => {
    vi.restoreAllMocks();
  });

  it('exports a configuration with values from environment', () => {
    vi.mock('dotenv', () => {
      return {
        config: vi.fn().mockImplementation(() => {
          vi.stubEnv('APP_CLIENT_ID', 'test-client-id');
          vi.stubEnv('APP_CLIENT_SECRET', 'test-client-secret');
          vi.stubEnv('APP_HOSTNAME', 'test-hostname');
          vi.stubEnv('APP_PORT', '9999');
          vi.stubEnv('APP_PROTOCOL', 'http');
          vi.stubEnv('APP_WALLET_BASE_URL', 'https://wallet.example.com/');
          vi.stubEnv('APP_CONSUMER_WEBID', 'https://idp.example.com/consumer/profile/card#me');
          vi.stubEnv('APP_PROVIDER_WEBID', 'https://idp.example.com/provider/profile/card#me');
        })
      }
    });

    expect(config.APP_CLIENT_ID).toBe('test-client-id');
    expect(config.APP_CLIENT_SECRET).toBe('test-client-secret');
    expect(config.APP_HOSTNAME).toBe('test-hostname');
    expect(config.APP_PORT).toBe('9999');
    expect(config.APP_PROTOCOL).toBe('http');
    expect(config.APP_WALLET_BASE_URL).toBe('https://wallet.example.com/');
    expect(config.APP_CONSUMER_WEBID).toBe('https://idp.example.com/consumer/profile/card#me');
    expect(config.APP_PROVIDER_WEBID).toBe('https://idp.example.com/provider/profile/card#me');
  });
});
